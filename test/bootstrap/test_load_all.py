#!/bin/python

"""This test will try to require every version of every module"""
from __future__ import print_function
import argparse
import sys
import os
import subprocess
import logging

RERUN_FILENAME = 'rerun.list'
ENV_VARS = ['EPICS_BASE', 'EPICS_MODULES_PATH', 'EPICS_BASES_PATH']

def eprint(msg):
    """Print to stderr"""
    print(msg, file=sys.stderr)

def supports_color():
    """
    Returns True if the running system's terminal supports color, and False
    otherwise."""
    plat = sys.platform
    supported_platform = plat != 'Pocket PC' and (plat != 'win32' or
                                                  'ANSICON' in os.environ)
    # isatty is not always implemented, #6223.
    is_a_tty = hasattr(sys.stdout, 'isatty') and sys.stdout.isatty()
    if not supported_platform or not is_a_tty:
        return False
    return True

def broken_modules(path):
    """TODO: Remove this function..."""
    yield ('ecmc', 'torstenbogershausen')
    yield ('scanning', 'alex')
    yield ('scanning', '2.0.0')
    yield ('sis8300bpm', '1.7.0')
    yield ('sis8300bpm', 'hinkokocevar')
    yield ('adpluginfits', '1.0')
    yield ('pscpt', '1.3.1')
    yield ('pscpt', 'alex')
    yield ('sis8300drv', '1.5.2')
    yield ('sis8300bcm', '1.4.3')
    yield ('sscan', 'alex')
    yield ('singlemotion', '1.3.0')
    yield ('singlemotion', '1.3.3')

def modules_failed(filename):
    """Generate to produce previously failed modules"""
    with open(filename, 'r') as rerunfile:
        for line in rerunfile:
            yield line.strip().split(',')

def modules(path):
    """Generator to produce module and version tuples"""
    base = os.getenv('EPICS_BASE').rsplit('-', 1)[1]
    arch = os.getenv('EPICS_HOST_ARCH')
    path_len = len(path.split(os.sep))
    for (root, dirs, _) in os.walk(path):
        if len(root.split(os.sep)) != path_len + 1:
            continue
        for ent in dirs:
            module = root.rsplit(os.sep, 1)[1]
            if os.path.isfile(os.path.join(root, ent, base, 'lib', arch, '{}.dep'.format(module))):
                yield (module, ent)

def test(module, version):
    """Individual test"""
    cmd = 'iocsh -r {},{} --frozen'.format(module, version)
    process = subprocess.Popen(
        cmd,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    (stdout, stderr) = process.communicate()
    status = process.returncode
    if status != 0:
        return (status, stdout.split('\n')[:-1], stderr.split('\n')[:-1])
    return None

def format_test_result(res):
    """Format test result"""
    (out, err) = res[:]
    eprint('stdout[-10:]:')
    for line in out[-10:]:
        eprint('{}'.format(line))
    eprint('stderr:')
    for line in err:
        if supports_color():
            eprint('\x1b[31m{}\x1b[0m'.format(line))
        else:
            eprint('{}'.format(line))

def setup():
    """Setup"""
    logger = logging.getLogger(__name__)
    for var in ENV_VARS:
        if not os.getenv(var):
            logger.error('Variable missing: {}'.format(var), file=sys.stderr)
            return -1
    os.putenv("REQUIRE_LOCK", "NO")
    return 0

def test_int(report, only_rerun):
    """Test"""
    logger = logging.getLogger(__name__)
    mods_path = os.getenv('EPICS_MODULES_PATH')
    success_rate = 0
    fail_rate = 0
    rerun = []
    if only_rerun:
        mods = lambda: modules_failed(RERUN_FILENAME)
    else:
        mods = lambda: modules(mods_path)
    for (module, version) in mods():
        logger.debug('Requiring {},{}'.format(module, version))
        res = test(module, version)
        if res:
            fail_rate += 1
            report.write('Loading {},{} failed with: {}\n'.format(module, version, res[0]))
            format_test_result(res[1:])
            rerun.append('{},{}\n'.format(module, version))
        else:
            success_rate += 1
            report.write('Loading {},{} succeeded!\n'.format(module, version))

    with open(RERUN_FILENAME, 'w') as rerunfile:
        for line in rerun:
            rerunfile.write(line)

    report.write('Ok/Failed/Total {}/{}/{}\n'.format(success_rate, fail_rate, success_rate+fail_rate))
    return 0

def test_rerun(report):
    """Only rerun the tests that failed before"""
    test_int(report, True)

def test_all(report):
    """Run all tests"""
    test_int(report, False)

def main(args):
    """Main function"""
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    args = parser.parse_args(args)

    logging.basicConfig(format='%(filename)s: %(message)s')
    if args.debug:
        logging.getLogger(__name__).setLevel(logging.DEBUG)

    status = setup()
    if status != 0:
        return status

    with sys.stdout as logfile:
        if os.path.isfile(RERUN_FILENAME):
            status = test_rerun(logfile)
        else:
            status = test_all(logfile)
        if status != 0:
            return status

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
