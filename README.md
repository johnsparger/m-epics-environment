# EPICS Environment
Read `scripts/module.Makefile` for more details.

## Known issues

* Header files conflicting with EPICS Base are not allowed. module.Makefile
  might inadvertently "update" the EPICS base header file to the one in the
  module.

## CHANGELOG
### Version Next

### Version 2.0.0
- Breaking changes
    * Require version in the `require` command
    * Rename atbuild.sh to atbuild
- General
    * Update systemd service files
    * Add a new test to load all versions of all installed modules
- Features
    * Preserve mode for executables when installing
    * Make it easier to generate the profile.d file
    * Add a script to clean up procServ logs
    * Minus character can now be used in project names
    * Detect untracked and staged files and install as named version
    * Implement lockfiles for iocsh
    * Implement immediate exit in iocsh, `iosch --frozen`
    * Add special treatment to -ESS[N] label. New priority order: 1.0-ESS0 > 1.0 > 1.0-alpha
- Bugfixes
    * Fix USR_DEPENDENCIES
    * Install from non-git directory
    * require.c: failed to load module in some edge cases
    * bootstrap: MSI host changed to https
    * Fix SNL recompilation bug
    * Other minor bugfixes

### Version 1.8.2
- Features
    * Add the possibliity to install OS specific headers.
    * Add --base flag to eeebake so that eeebake can install only one
      base version of all modules.
    * Add install.<epics-version> to module.Makefile to be able to install
      only one version.
    * If one compiles 3.15 first, and therefore use the 3.15 rules for substitution
      expansion rules, it will now recompile when any included template file change
    * Immediately start IOC when running in GDB (iocsh -d)
- Bugfixes
    * Fix template compilation with 3.15 rules
    * Fix issue with module overwriting base headers
    * Minor bug fixes to eeebake
    * Allow options -d, -dp and -dv not only as first option in iocsh

### Version 1.8.1
- Bugfixes
    * Fix sequencer preprocessing bug

### Version 1.8.0
- General
    * Improvements to bootstrapping scripts
    * Bootstrapping scripts won't read values from environment any more
- Features
    * Improve compile speed by reducing dependency calculation
    * Add support for pre-release in tags (-beta, -alpha, -rc0, ...)
    * Pick tag with highest precedence if multiple tags point to the same commit
- Bugfixes
    * Disable auto dependency on anything other than MAJOR.MINOR.PATCH
    * Fix partial recompiles which sometimes failed on external headers

### Version 1.7.2
- Bugfixes
    * Fix minor stuff with bootstrapping process

### Version 1.7.1
- General
    * Add SIS8300LLRF, Area Detector and other modules to bootstrap script
- Features
    * Output error message if requireExec fails to find module
- Bugfixes
    * Fix autodetection of sources
    * Fix failed to detect dependency on sequencer
    * Fix finding MISCS files

### Version 1.7.0
- Features
    * Add verbose option to requireExec
    * Add support for EPICS V4 modules
    * Extend support for carelessly written DBDs
    * Add environment variables for each loaded library
- Bugfixes
    * Improve support for EPICS specific sources

### Version 1.6.1
- Bugfixes
    * Fix ioc command requireExec issue with stdin
- Features
    * Add more output from requireExec

### Version 1.6.0
- General
    * Initial version of inotify modules_watcher.py
- Features
    * Add verbose option to requireExec
    * Improve bash completion for requireExec

### Version 1.5.0
- General
    * Add systemd unit files
    * Add ess boot script
    * Add helper script for yocto to generate ess_epics_env.sh
- Bugfixes
    * Fix requireExec issue with system libraries
- Features
    * Integrate bash completions for requireExec in ess_epics_env.sh

### Version 1.4.1
- Bugfixes
    * Fix issue with environment in requireExec

### Version 1.4
- Features
    * Implement requireExec application
    * Compile library before executables.

### Version 1.3.1
- Bugfixes:
    * Fix issue with requireSnippet

### Version 1.3
- General:
    * Adding caput/camonitor to path in bootstrapping.
- Bugfixes:
    * Fix OS\_CLASS specific sources, e.g. SOURCES\_Linux = source.c
- Features
    * requireExec(executable, args, outfile, assertNoPath)
    * DOC variables supports paths (like OPIS)
    * Added support for folders for the DOC variable.

### Version 1.2
- General:
    * bootstrapping scripts now fetch epics from bitbucket
- Bugfixes:
    * Fixed compilation issue with only OPI module
- Features:
    * Added support for requireSnippet in EPICS shell.

### Version 1.1
- Features:
    * Added support for OPI files

### Version 1.0.1
- General:
    * General improvements to bootstrapping scripts
    * Added support for ELDK Kernel to helper script atbuild.sh
    * Added documentation
- Bugfixes:
    * get\_version.py: Don't fail if git is unavailable.
    * iocsh: load the correct environment software library.
- Features:
    * Calculate dependencies on d-files instead of h-files.
