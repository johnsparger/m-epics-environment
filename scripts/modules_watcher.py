#!/usr/bin/env python

"""Listen to modifications on /opt/epics/modules

If a module directory or a version directory is created we set the correct permissions after waiting a while.
"""

from __future__ import print_function
import os
import sys
import argparse
import subprocess, shlex
import re
import threading
import time


DEVNULL = open('/dev/null', 'w')

class ModulesWatcher(object):
    """Watch the modules directory for new modules and versions of modules"""
    def __init__(self):
        self.outfile = None
        self.modules_path = None

    def main(self, arguments):
        """Main function"""
        parser = argparse.ArgumentParser(description=__doc__,
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('--modules-path', help='Path to modules', default='/opt/epics/modules')
        parser.add_argument('-o', '--outfile', help="Output file",
                            default=sys.stdout, type=argparse.FileType('w'))

        args = parser.parse_args(arguments)
        self.outfile = args.outfile
        self.modules_path = os.path.abspath(args.modules_path)

        command = shlex.split('inotifywait -e create -r -m {} --format "%T,%:e,%w%f" --timefmt "%F %H:%M:%S"'.format(args.modules_path))
        monitor = subprocess.Popen(command, stdout=subprocess.PIPE)

        while True:
            self.process_line(monitor.stdout.readline())
            if monitor.poll() != None:
                args.outfile.write('inotifywait died\n')
                break

    def process_line(self, line):
        """Processes a line from inotifywait."""
        line_split = line[0:-1].split(',')
        if len(line_split) != 3:
            print('Broken formatting')
            return
        (timestamp, event, filename) = line_split
        filename_rel = filename[len(self.modules_path):]

        matches = re.match(r'/([^/]*)$', filename_rel)
        if matches:
            # Created a new module, since inotifywait is racy we might have to
            # handle the case when there is no notification of a new version.
            # Inotifywait will attach to the module directory as fast as it can,
            # but it might attach after the fact that the version directory is created.
            mname = matches.group(1)
            threading.Thread(target=self.delayed_check, args=(mname, 1)).start()

        matches = re.match(r'/([^/]*)/([^/]*)$', filename_rel)
        if matches:
            self.process_new_version(matches.group(1), matches.group(2))

    def delayed_check(self, mname, wait=10):
        """Wait $time seconds and then check if there is an incorrectly installed version."""
        time.sleep(wait)
        for mversion in os.listdir(os.path.join(self.modules_path, mname)):
            self.process_new_version(mname, mversion)

    def process_new_version(self, mname, mversion):
        """Process a module version."""
        path = os.path.join(self.modules_path, mname, mversion)
        installer_uid = os.stat(path).st_uid
        self.outfile.write('Module {} ({}) installed by id {}\n'.format(mname, mversion, installer_uid))
        if not self.valid_owner_version(mname, mversion):
            self.outfile.write('Will rename in 5 seconds!\n')
            threading.Thread(target=self.delayed_mv, args=(mname, mversion, 5)).start()

    def valid_owner_version(self, mname, mversion):
        """Return true if it is a named version OR numbered version and owner is root."""
        path = os.path.join(self.modules_path, mname, mversion)
        installer_uid = os.stat(path).st_uid
        matches = re.match(r'(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)$', mversion)
        return (not matches) or (matches and installer_uid == 0)

    def delayed_mv(self, mname, mversion, wait=10):
        """Rename numbered version to illegal[number]_<version>"""
        time.sleep(wait)
        mversion_from = os.path.join(self.modules_path, mname, mversion)
        mversion_to = os.path.join(self.modules_path, mname, 'illegal_{}'.format(mversion))
        i = 0
        while os.path.isdir(mversion_to):
            self.outfile.write('Already exists: {}\n'.format(mversion_to))
            mversion_to = os.path.join(self.modules_path, mname, 'illegal{}_{}'.format(i, mversion))
            i = i + 1
        subprocess.call(shlex.split('mv {} {}'.format(mversion_from, mversion_to)), stdout=DEVNULL)
        self.outfile.write('Rename {} to {}\n'.format(mversion_from, mversion_to))

if __name__ == '__main__':
    try:
        sys.exit(ModulesWatcher().main(sys.argv[1:]))
    except KeyboardInterrupt:
        sys.exit(0)
