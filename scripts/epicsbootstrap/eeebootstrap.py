#!/bin/env python2.7
#
# EPICS Environment Manager
# Copyright (C) 2015 Cosylab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This script prepares the computer for building EPICS modules.
Works on red hat derived linux machines.
"""

from __future__ import print_function
import subprocess
import os
import tempfile
import re
import json
import ConfigParser
import platform
import argparse
import shlex
import shutil
import datetime
import logging

FNULL = None
LOGDIR = os.path.join(tempfile.gettempdir(), 'eeebootstrap', 'log')
WORKDIR = os.path.join(tempfile.gettempdir(), 'eeebootstrap', 'work')
SCRIPTDIR = os.path.dirname(os.path.realpath(__file__))


def is_centos7():
    """Returns true if system is centos7"""
    return platform.dist()[0] == 'centos' and platform.dist()[1].startswith('7')

def do_install_dependencies(): # pylint: disable=too-many-branches
    """Install system dependencies."""
    print('Installing system dependencies')
    packages = []
    # pvAccess dependency
    if not os.path.isfile('/usr/include/python2.7/pyconfig.h'):
        packages.append('python-devel')
    # Linux driver dependency
    if not os.path.isfile('/usr/include/libudev.h'):
        packages.append('systemd-devel')
    # EPICS Base dependency
    if not os.path.isfile('/usr/include/readline/readline.h'):
        packages.append('readline-devel')
    # NDS 2 dependency
    if not os.path.isdir('/usr/include/boost'):
        packages.append('boost-devel')
    # Sequencer dependency
    if subprocess.call(['which', 're2c'], stderr=FNULL, stdout=FNULL) != 0:
        if is_centos7():
            system('yum install -y '
                   'http://dl.fedoraproject.org/pub/fedora/linux/releases/22/Everything/x86_64/os/Packages/r/'
                   're2c-0.13.5-9.fc22.x86_64.rpm', True)
        else:
            packages.append('re2c')
    # NDS 2 dependency
    if not os.path.isdir('/usr/include/curl'):
        packages.append('curl-devel')
    # EPICS Base dependency
    if subprocess.call(['which', 'xsubpp'], stderr=FNULL, stdout=FNULL) != 0:
        if is_centos7():
            packages.append('perl-ExtUtils-ParseXS')
        else:
            packages.append('perl-devel')
    # EPICS Base dependency
    if subprocess.call(['which', 'podchecker'], stderr=FNULL, stdout=FNULL) != 0:
        if is_centos7():
            packages.append('perl-Pod-Checker')
        else:
            packages.append('perl')
    # AreaDetector dependency
    if not os.path.isfile('/usr/include/hdf5.h'):
        packages.append('hdf5-devel')
    # AreaDetector dependency
    if not os.path.isfile('/usr/include/jpeglib.h'):
        packages.append('libjpeg-turbo-devel')
    # AreaDetector dependency
    if not os.path.isfile('/usr/include/tiffio.h'):
        packages.append('libtiff-devel')

    system('yum groupinstall -y "Development tools"', os.geteuid() != 0)
    system('yum install -y epel-release', os.geteuid() != 0)
    if packages:
        system('yum install -y {}'.format(' '.join(packages)), os.geteuid() != 0)


def do_write_profile(profile_template, epics_bases_path, epics_modules_path, epics_host_arch,
                     epics_base_version, epics_pvaccesscpp_version, epics_pvapy_version):
    """Write the profile file so that future users will get the correct environment variables.
    If they were not set, set them for this session as well.
    """
    profilename = os.path.join(WORKDIR, 'ess_epics_env.sh')

    with open(profilename, 'w') as profilefile, open(profile_template) as templatefile:
        template = templatefile.read()
        profilefile.write(template.format(epics_host_arch=epics_host_arch,
                                          epics_bases_path=epics_bases_path,
                                          epics_modules_path=epics_modules_path,
                                          epics_base_version=epics_base_version,
                                          epics_env_version=get_eee_version(),
                                          epics_pvaccesscpp_version=epics_pvaccesscpp_version,
                                          epics_pvapy_version=epics_pvapy_version))
    install(profilename, '/etc/profile.d/ess_epics_env.sh')

def do_fetch_epics(epics_bases_path, epics_vers, epics_repo):
    """Download all listed EPICS bases into the temoporary working directory
    and untar them into the correct installation path. Finally it will set
    some EPICS build parameters.
    """
    mkdir(epics_bases_path)

    sudo = not os.access(epics_bases_path, os.W_OK)

    for epics_version in epics_vers:
        print('Cloning {}'.format(epics_version))
        if not os.path.isdir(os.path.join(epics_bases_path, 'base-{}'.format(epics_version))):
            git_branch = 'v{}'.format(epics_version)
            git_dir = os.path.join(epics_bases_path, 'base-{}'.format(epics_version))
            system('git clone -b {} {} {}'.format(git_branch, epics_repo, git_dir), sudo)

    set_epics_build_param('CONFIG_SITE', 'USE_POSIX_THREAD_PRIORITY_SCHEDULING', 'YES', epics_bases_path)
    set_epics_build_param('CONFIG_SITE', 'EPICS_SITE_VERSION', datetime.date.today().strftime('%Y-%m'),
                          epics_bases_path)
    set_epics_build_param('CONFIG_SITE_ENV', 'EPICS_TIMEZONE', 'CET/CEST::-60:032902:102502', epics_bases_path)

def do_add_targets(epics_bases_path, epics_host_arch, targets=None, templates=None):
    """For every installed EPICS base it will install the specified targets."""
    if targets is None:
        targets = []
    if templates is None:
        templates = {}
    generic_epics_arch = '{}-{}'.format(platform.system().lower(), platform.machine())
    for epics_base in get_installed_epics_bases(epics_bases_path):
        sudo = not os.access(os.path.join(epics_base, 'configure'), os.W_OK)

        for target in [get_epics_host_arch()] + targets:
            target_path = os.path.join(epics_base, 'configure', 'os', 'CONFIG.Common.{}'.format(target))
            if target in templates.keys():
                system('cp -f {} {}'.format(os.path.join(SCRIPTDIR, templates[target]), target_path), sudo)
            else:
                print('ERROR: No template configured for {}'.format(target))

        for name_template in ('CONFIG.{}.Common',
                              'CONFIG.{0}.{0}',
                              'CONFIG_SITE.Common.{}',
                              'CONFIG_SITE.{}.Common'):
            (temp, tempname) = tempfile.mkstemp()
            os.fchmod(temp, 0644)
            with os.fdopen(temp, 'w') as tempfh:
                tempfh.write('include $(CONFIG)/os/{}\n'.format(name_template.format(generic_epics_arch)))
            system('cp -f {} {}'.format(tempname,
                                        os.path.join(epics_base,
                                                     'configure',
                                                     'os',
                                                     name_template.format(epics_host_arch))), sudo)
            os.remove(tempname)

    set_epics_build_param(os.path.join('os', 'CONFIG_SITE.{}.Common'.format(epics_host_arch)),
                          'CROSS_COMPILER_TARGET_ARCHS',
                          ' '.join(targets), epics_bases_path)

def do_compile_epics(jobs, epics_bases_path):
    """For every installed EPICS base, compile it."""
    logging.getLogger(__name__).debug(get_epics_host_arch())
    for epics_base in get_installed_epics_bases(epics_bases_path):
        print("Compiling EPICS {}".format(epics_base))
        sudo = not os.access(epics_base, os.W_OK)
        system('make install -j{} -C {}'.format(jobs, epics_base), sudo)

def do_compile_msi(msi, epics_bases_path):
    """For every 3.14 install MSI."""
    msi_name = os.path.basename(msi)
    if not os.path.isfile(os.path.join(WORKDIR, msi_name)):
        system('curl -O {}'.format(msi))
    for epics_base in get_installed_epics_bases(epics_bases_path, '3.14'):
        sudo = not os.access(epics_base, os.W_OK)
        extensions = os.path.join(epics_base, 'extensions')
        mkdir(extensions)
        if not os.path.isdir(os.path.join(extensions, msi_name.split('.')[0])):
            system('tar xzf {} -C {}'.format(os.path.join(WORKDIR, msi_name), extensions), sudo)
        system('make -C {}'.format(os.path.join(epics_base, 'extensions', msi_name.split('.')[0])), sudo)

def do_install_eee(epics_modules_path):
    """Install the first EPICS module (environment)"""
    modules_path = epics_modules_path
    mkdir(modules_path)
    sudo = not os.access(modules_path, os.W_OK)
    print('Installing "{}" as version "{}"'.format('environment', get_eee_version()))
    system('make -C {}/../../ install'.format(SCRIPTDIR), sudo)


def get_eee_version():
    """Returns EEE version based on 'make version' in current repository"""
    return subprocess.check_output(
        shlex.split('make --no-print-directory -C {}/../../ version'.format(SCRIPTDIR))).strip()


def set_epics_build_param(config_file, name, value, epics_bases_path):
    """Set 'name' to 'value' in all installed EPICS bases CONFIG_SITE."""
    for epics_base in get_installed_epics_bases(epics_bases_path):
        config_site_name = os.path.join(epics_base, 'configure', config_file)
        (temp, tempname) = tempfile.mkstemp()
        os.fchmod(temp, 0644)
        with open(config_site_name, 'r') as config_site, os.fdopen(temp, 'w') as tempfh:
            found_line = False
            for line in config_site:
                (newline, num_subs) = re.subn(r'^({}).*'.format(name), r'\1 = {}'.format(value), line)
                tempfh.write(newline)
                if num_subs > 0:
                    found_line = True
            if not found_line:
                tempfh.write('{} = {}\n'.format(name, value))

        install(tempname, config_site_name)
        os.remove(tempname)


def install(src, target):
    """Install a file if permitted, otherwise try as sudo."""
    if os.access(os.path.dirname(target), os.W_OK):
        shutil.copy2(src, target)
    else:
        print('Using super user privileges to install {}'.format(target))
        subprocess.check_call(['sudo', 'cp', '-f', src, target])

def system(cmd, sudo=False):
    """Run a command. Set 'sudo' to True to run as sudo."""
    try:
        commandline = shlex.split(cmd)
        logging.getLogger(__name__).debug(commandline)
        if sudo:
            subprocess.check_output(['sudo', '-E'] + commandline,
                                    cwd=WORKDIR, stderr=subprocess.STDOUT, env=os.environ.copy())
        else:
            subprocess.check_output(commandline, cwd=WORKDIR, stderr=subprocess.STDOUT, env=os.environ.copy())
        return 0
    except subprocess.CalledProcessError as why:
        logname = os.path.join(LOGDIR, 'run.log')
        with open(logname, 'a') as logfile:
            logfile.write(why.output)
            print('ERROR: "{}" failed with {}, see logfile: {}'.format(why.cmd, why.returncode, logname))
        raise SystemExit(why.returncode)
        #return why.returncode

def mkdir(path):
    """Create a directory recursively."""
    if os.path.isdir(path):
        return
    try:
        os.makedirs(path)
    except OSError:
        try:
            print('Using super user privileges to create {}'.format(path))
            subprocess.check_output(['sudo', '-E', 'mkdir', '-p', path])
        except subprocess.CalledProcessError as why:
            logname = os.path.join(LOGDIR, 'run.log')
            with open(logname, 'a') as logfile:
                logfile.write(why.output)
                print('ERROR: "{}" failed with {}, see logfile: {}'.format(why.cmd, why.returncode, logname))

def get_installed_epics_bases(epics_bases_path, basefilter='base'):
    """Returns a list of paths to all installed EPICS bases.
    If basefilter is given, only bases which match the filter is returned.
    """
    if epics_bases_path:
        if os.path.isdir(epics_bases_path):
            return [os.path.join(epics_bases_path, x) for x in os.listdir(epics_bases_path)
                    if basefilter in x]
        else:
            print("ERROR: {} is not a path".format(epics_bases_path))
    return []

def get_epics_host_arch():
    """Try to figure out the host architecture."""
    (dist, version, _) = platform.linux_distribution()
    dist = dist.lower()
    if dist:
        if 'centos' in dist:
            return 'centos{}-{}'.format(version[0], platform.machine())
        elif 'scientific linux' in dist:
            return 'SL{}-{}'.format(version[0], platform.machine())
        elif dist in ('ubuntu', 'debian'):
            return '{}{}-{}'.format(dist, version[0], platform.machine())
    return '{}-{}'.format(platform.system().lower(), platform.machine())

def get_option_or(config, section, option, default):
    """Get option from configuation or return default value"""
    try:
        return config.get(section, option)
    except ConfigParser.NoOptionError:
        return default

def get_option(config, section, option):
    """Get option from configuration or raise SystemExit"""
    try:
        return config.get(section, option)
    except ConfigParser.NoOptionError as why:
        raise SystemExit('Configure {}, {}, Exception: {}'.format(section, option, why))


def main():
    """Main function"""

    parser = argparse.ArgumentParser(description='Bootstrap the EPICS environment')
    parser.add_argument('-j', default=2, type=int, help='Number of Make threads')
    parser.add_argument('--debug', '-d', action='store_true', help='Enable debug output.')
    parser.add_argument('--config', '-c', help='Specify a non-standard config file',
                        default='epicsbootstrap.cfg')
    args = parser.parse_args()

    if args.debug:
        logging.getLogger(__name__).setLevel(logging.DEBUG)

    config = ConfigParser.ConfigParser()
    with open(args.config) as configfile:
        config.readfp(configfile)

    profile_template = get_option(config, 'eeebootstrap', 'profile_template')

    epics_bases_path = get_option(config, 'global', 'epics_bases_path')
    os.environ['EPICS_BASES_PATH'] = epics_bases_path

    epics_modules_path = get_option(config, 'global', 'epics_modules_path')
    os.environ['EPICS_MODULES_PATH'] = epics_modules_path

    do_install_dependencies()

    epics_host_arch = get_option_or(config, 'eeebootstrap', 'host_arch', get_epics_host_arch())
    os.environ['EPICS_HOST_ARCH'] = epics_host_arch

    epics_versions = json.loads(get_option(config, 'eeebootstrap', 'epics_versions'))

    default_epics_version = get_option_or(config, 'eeebootstrap', 'default_epics_version', epics_versions[0])
    default_pvaccesscpp_version = get_option(config, 'eeebootstrap', 'default_pvaccesscpp_version')
    default_pvapy_version = get_option(config, 'eeebootstrap', 'default_pvapy_version')


    do_write_profile(profile_template, epics_bases_path, epics_modules_path, epics_host_arch, default_epics_version,
                     default_pvaccesscpp_version, default_pvapy_version)

    server = get_option(config, 'global', 'server')
    epics_repository = get_option(config, 'eeebootstrap', 'epics_repository')
    epics_repo = '{}{}'.format(server, epics_repository)

    do_fetch_epics(epics_bases_path, epics_versions, epics_repo)

    try:
        targets = json.loads(config.get('eeebootstrap', 'targets'))
    except ConfigParser.NoOptionError:
        targets = []

    try:
        target_templates = json.loads(config.get('eeebootstrap', 'target_templates'))
    except ConfigParser.NoOptionError:
        target_templates = {}

    do_add_targets(epics_bases_path, epics_host_arch, targets, target_templates)
    do_compile_epics(args.j, epics_bases_path)

    msi = get_option(config, 'eeebootstrap', 'msi_tarball')

    do_compile_msi(msi, epics_bases_path)
    do_install_eee(epics_modules_path)

    print("""Now you are ready to use the EPICS Environment.
If you want to continue to use this shell, please source:
    {}
It will be automatically sourced the next time you login.
    """.format('/etc/profile.d/ess_epics_environment.sh'))

if __name__ == '__main__':
    logging.basicConfig(format='%(filename)s: %(message)s')

    # Create temporary work/log dirs
    if not os.path.isdir(WORKDIR):
        os.makedirs(WORKDIR)
    if not os.path.isdir(LOGDIR):
        os.makedirs(LOGDIR)

    with open(os.devnull, 'w') as FNULL:
        main()
