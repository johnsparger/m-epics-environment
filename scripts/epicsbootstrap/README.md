# Bootstrap EPICS

This set of scripts will install EPICS with our dependencies on a RHEL based
Linux machine.

Tested with:
* Scientific Linux 6
* Centos 7

Check that `eeebootstrap.cfg` and `eeebake.cfg` contain the configuration you want to use.

```
python eeebootstrap.py -j12
python eeebake.py -j12
```
