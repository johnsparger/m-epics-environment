#!/bin/env python2.7
#
# EPICS Environment Manager
# Copyright (C) 2015 Cosylab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# pulint: disable=global-statement

"""This script compiles all epics modules specified in the config file"""

from __future__ import print_function

import ConfigParser
import json
import os
import subprocess
import threading
import Queue
import argparse
import signal
import tempfile
import logging
import re
import time

WORKDIR = os.path.join(tempfile.gettempdir(), 'eeebake', 'work')
LOGDIR = os.path.join(tempfile.gettempdir(), 'eeebake', 'log')

def format_version(tag):
    """Converts a tag into an EPICS Module version."""
    tag_stripped = tag.lstrip('v')
    if not re.match(r'^[0-9A-Za-z\.\-+]*$', tag_stripped):
        print('Found illegal characters in {}'.format(tag))
        return None
    if '+' in tag_stripped:
        (tag_stripped, metadata) = tag_stripped.rsplit('+', 1)
    if '-' in tag_stripped:
        (tag_stripped, prerelease) = tag_stripped.rsplit('-', 1)
    parts = tag_stripped.split('.')
    if len(parts) == 1:
        try:
            int(parts[0])
            return '{}.0.0'.format(parts[0])
        except ValueError:
            return None
    if len(parts) == 2:
        try:
            int(parts[0])
            int(parts[1])
            return '{}.{}.0'.format(parts[0], parts[1])
        except ValueError:
            return None
    if len(parts) == 3:
        try:
            int(parts[0])
            int(parts[1])
            int(parts[2])
            return tag_stripped
        except ValueError:
            return None
    return None

class Worker(threading.Thread):
    """Generic worker class"""
    def __init__(self, wid, tasks):
        self._wid = wid
        self._tasks = tasks
        self._stop = False
        super(Worker, self).__init__()

    def run(self):
        """Get jobs from the queue and process them"""
        logging.getLogger(__name__).debug('Worker {} starting'.format(self._wid))
        jobs_done = 0
        while not self._stop:
            try:
                (cmd, args, callback) = self._tasks.get(True, 5)
            except Queue.Empty:
                # No more jobs in queue
                break
            jobs_done += 1
            print('Job {:2}-{:03} : {} {}'.format(self._wid, jobs_done, cmd.__name__, args))
            if callback is None:
                cmd(*args)
            else:
                callback(*cmd(*args))
        logging.getLogger(__name__).debug('Worker {} finished'.format(self._wid))

    def go_away(self):
        """Tell thread to stop"""
        self._stop = True

class WorkerPool(object):
    """Generic worker pool"""
    def __init__(self, num_threads):
        self._threads = []
        self._tasks = Queue.Queue()
        for i in range(num_threads):
            thread = Worker(i, self._tasks)
            self._threads.append(thread)

    def stop_all(self):
        """Stop all threads"""
        for thread in self._threads:
            thread.go_away()

    def join_all(self):
        """Join all workers"""
        # Poll threads since join is un-interruptable...
        while any([thread.is_alive() for thread in self._threads]):
            time.sleep(1)
        for thread in self._threads:
            thread.join()

    def add_task(self, task, args=None, task_callback=None):
        """Add task"""
        if not callable(task):
            raise Exception('Task is not callable')
        self._tasks.put((task, args, task_callback))

    def start_all(self):
        """Start all workers"""
        for thread in self._threads:
            thread.start()

def main():
    """Main function"""
    def do_fetch(src):
        """Clones or fetches the latest sources."""
        logging.getLogger(__name__).debug('fetch {}'.format(src))
        module = os.path.basename(src)
        logname = 'fetch_{}'.format(module)
        stdout = open(os.path.join(LOGDIR, logname), 'w')
        if os.path.isdir(os.path.join(WORKDIR, module)):
            do_clean(module)
            command = 'git checkout -q master && git fetch -p -t -q && git merge -q origin/master'
            workdir_module = os.path.join(WORKDIR, module)
        else:
            command = 'git clone -b master -q {}'.format(src)
            workdir_module = WORKDIR
        returncode = subprocess.call(command, cwd=workdir_module, shell=True, stdout=stdout)
        if returncode != 0:
            print('Failed to checkout {}, check logfile {}'.format(src, logname))
            stdout.close()
            return False
        stdout.close()
        return True

    def do_look(module, versions, install_all, atbuild, base_version=None):
        """Lists tags to determine what to build."""
        command = 'git for-each-ref --sort=taggerdate --format \'%(refname:short)\' refs/tags'
        workdir_module = os.path.join(WORKDIR, module)
        proc = subprocess.Popen(command, cwd=workdir_module, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = proc.communicate()
        found_tags = out.strip().split('\n')
        # Remove tags that are not valid versions
        found_tags = [x for x in found_tags if format_version(x)]
        tags = []
        if proc.returncode != 0:
            print('Failed with exit code {} ({})'.format(proc.returncode, err))
            return False
        if install_all:
            tags.extend(found_tags)
        elif versions:
            for version in versions:
                if version in found_tags:
                    tags.append(version)
            if "latest" in versions and found_tags[-1] not in tags:
                tags.append(found_tags[-1])
        else:
            tags.append(found_tags[-1])

        logging.getLogger(__name__).debug('Tags requested: {}, tags available: {}'.format(tags, found_tags))

        if tags:
            if atbuild:
                return (do_install_autotool, (module, tags, base_version))
            else:
                return (do_install_regular_module, (module, tags, base_version))

    def do_clean(module):
        """Remove all non version controlled files"""
        command = 'git clean -x -d -q -f -e "*tar*"'
        workdir_module = os.path.join(WORKDIR, module)
        subprocess.call(command, cwd=workdir_module, shell=True)

    def do_checkout(module, tag, workdir):
        """Checkout tag"""
        logname = os.path.join(LOGDIR, 'checkout_{}'.format(module))
        with open(logname, 'w') as log:
            returncode = subprocess.call('git checkout {}'.format(tag), cwd=workdir, shell=True, stdout=log,
                                         stderr=log)
            if returncode != 0:
                print('Failed with exit code {} (log file is located at {})'.format(returncode, logname))
                return False
        return True

    def do_precompile(module, tag, workdir, base_version):
        """Check if module version already is installed"""
        logger = logging.getLogger(__name__)
        logname = os.path.join(LOGDIR, 'precompile_{}'.format(module))
        with open(logname, 'w') as log:
            instver_proc = subprocess.Popen('make list-verbose', cwd=workdir, shell=True, stderr=log,
                                            stdout=subprocess.PIPE)
            (out, _) = instver_proc.communicate()
            if instver_proc.returncode != 0:
                print('Failed with exit code {}, check logfile {}'.format(instver_proc.returncode, logname))
            else:
                if out.strip() == '-none-':
                    return True
                # "out" will contain entries like "4.21.0  3.14.12.5  centos7-x86_64".
                for installed_version in out.strip().split('\n'):
                    (m_ver, base, _) = installed_version.split()
                    if base_version:
                        if base == base_version and m_ver == format_version(tag):
                            logger.debug('Skipping {}'.format(tag))
                            return False
                    elif m_ver == format_version(tag):
                        logger.debug('Skipping {}'.format(tag))
                        return False
        return True

    def do_compile(module, tag, workdir, command):
        """Compile module"""
        logname = os.path.join(LOGDIR, 'compile_{}'.format(module))
        with open(logname, 'w') as log:

            logging.getLogger(__name__).debug('Compiling {} with "{}"'.format(tag, command))
            returncode = subprocess.call(command, cwd=workdir, shell=True, stdout=log, stderr=log)
            if returncode != 0:
                print('Failed with exit code {} (log file is located at {})'.format(returncode, logname))
                return False
        return True

    def do_install_regular_module(module, tags, base_version):
        """Compile a regular EPICS module."""
        logname = 'install_{}'.format(module)
        command = 'make -j {}'.format(makethreads)
        if base_version:
            command = command + ' build.{}'.format(base_version)
        _do_install(module, tags, logname, command, base_version)

    def do_install_autotool(module, tags, base_version):
        """Compile a module that uses autotool."""
        logname = 'installat_{}'.format(module)
        command = 'atbuild env.sh'
        if base_version:
            command = command + ' build.{}'.format(base_version)
        _do_install(module, tags, logname, command, base_version)

    def _do_install(module, tags, logname, compile_command, base_version):
        """Private function that actually compiles and installes both kinds of modules."""
        logger = logging.getLogger(__name__)
        workdir_module = os.path.join(WORKDIR, module)
        loginstallname = os.path.join(LOGDIR, logname)
        loginstall = open(loginstallname, 'w')
        for tag in tags:
            do_clean(module)

            if not do_checkout(module, tag, workdir_module):
                continue

            if not reinstall:
                if not do_precompile(module, tag, workdir_module, base_version):
                    continue

            if not do_compile(module, tag, workdir_module, compile_command):
                continue

            if reinstall:
                action = 'reinstall'
            else:
                action = 'install'
                if base_version:
                    action = 'install.{}'.format(base_version)

            if not os.access(os.environ['EPICS_MODULES_PATH'], os.W_OK):
                install_command = 'sudo EPICS_ENV_PATH=${{EPICS_ENV_PATH}} EPICS_MODULES_PATH=${{EPICS_MODULES_PATH}} '\
                          'EPICS_BASES_PATH=${{EPICS_BASES_PATH}} EPICS_HOST_ARCH=${{EPICS_HOST_ARCH}} '\
                          'EPICS_BASE=${{EPICS_BASE}} make {}'.format(action)
            else:
                install_command = 'make {}'.format(action)
            logger.debug('Installing {} with "{}"'.format(tag, install_command))
            returncode = subprocess.call(install_command, cwd=workdir_module, shell=True, stdout=loginstall,
                                         stderr=loginstall)
            if returncode != 0:
                print('Failed with exit code {} (log file is located at {})'.format(returncode, loginstallname))
                return
        loginstall.close()

    parser = argparse.ArgumentParser(description='Install all versions of all modules.')
    parser.add_argument('-j', default=1, type=int, help='Number of Make threads.')
    parser.add_argument('--all', action='store_true',
                        help='Install all versions of all modules (default only install latest).')
    parser.add_argument('--debug', '-d', action='store_true', help='Enable debug output.')
    parser.add_argument('--reinstall', action='store_true', help='Reinstall all modules.')
    parser.add_argument('--base', help='Install only a specific base. '\
                        '(WARNING: Will replace all base independent files.)')
    parser.add_argument('--config', '-c', help='Specify a non-standard config file',
                        default='epicsbootstrap.cfg')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig()
        logging.getLogger(__name__).setLevel(logging.DEBUG)


    reinstall = args.reinstall
    makethreads = args.j

    config = ConfigParser.ConfigParser()
    with open(args.config) as configfile:
        config.readfp(configfile)

    os.environ['EPICS_BASES_PATH'] = config.get('global', 'epics_bases_path')
    os.environ['EPICS_MODULES_PATH'] = config.get('global', 'epics_modules_path')

    atbuilds = json.loads(config.get('eeebake', 'atbuilds'))
    modules = json.loads(config.get('eeebake', 'modules'))

    print('{} modules to install'.format(len(modules) + len(atbuilds)))

    fetch_pool = WorkerPool(8)
    build_pool = WorkerPool(1) # Modules must be built in sequentially...

    def look_callback(*args):
        """Callback for do_look"""
        if len(args) == 2:
            build_pool.add_task(args[0], args[1])

    # Build the autotools modules first.
    for module in atbuilds:
        module_path = '{}{}'.format(config.get('global', 'server'), module['repo'])
        fetch_pool.add_task(do_fetch, (module_path,))
        build_pool.add_task(do_look,
                            (os.path.basename(module_path), module['versions'], args.all, True, args.base),
                            look_callback)

    # Build the EPICS modules.
    for module in modules:
        module_path = '{}{}'.format(config.get('global', 'server'), module['repo'])
        fetch_pool.add_task(do_fetch, (module_path,))
        build_pool.add_task(do_look,
                            (os.path.basename(module_path), module['versions'], args.all, False, args.base),
                            look_callback)

    print("Stop fetching/building at any time with CTRL-C")

    def signal_handler(signalnum, frame): #pylint: disable=unused-argument
        """Enable CTRL-C to stop building."""
        fetch_pool.stop_all()
        build_pool.stop_all()
    signal.signal(signal.SIGINT, signal_handler)

    fetch_pool.start_all()
    fetch_pool.join_all()

    build_pool.start_all()
    build_pool.join_all()



if __name__ == "__main__":
    if not os.path.isdir(WORKDIR):
        os.makedirs(WORKDIR)
    if not os.path.isdir(LOGDIR):
        os.makedirs(LOGDIR)

    main()
