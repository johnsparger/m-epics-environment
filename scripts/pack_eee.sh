#!/bin/bash

archive="epics_environment.tar.bz2"

usage() {
    echo "Pack EEE"
    echo "This script will pack EEE from \$EPICS_{MODULES,BASES}_PATH into ARCHIVE.tar.bz2"
    echo "Usage: $0 [-h] [ARCHIVE]"
    echo "  -h      - Print this help"
    echo "  ARCHIVE - Name of the archive to be created (default '${archive}')."
	echo "            Recognized extensions are: .gz, .tgz, .taz, .Z, .taZ, .bz2,"
	echo "            .tz2, .tbz2, .tbz, .lz, .lzma, .tlz, .lzo, .xz'."
    exit 0
}

if [ -z "$EPICS_BASES_PATH" -o -z "$EPICS_MODULES_PATH" ] ; then
	echo "ERROR: Missing env variable \$EPICS_BASES_PATH or \$EPICS_MODULES_PATH";
	echo ""
	usage
fi

if [ "$1" == "-h" -o "$1" == "--help" ] ; then
    usage
fi

if [ $# = 1 ] ; then
    archive="$1"
fi

profile_file=etc/profile.d/ess_epics_env.sh

echo "Compressing to ${PWD}/${archive}..."
tar --directory=/ --create --auto-compress --preserve-permissions --same-owner --file=${archive} ${EPICS_MODULES_PATH##/} ${EPICS_BASES_PATH##/} \
	${profile_file}
echo "Done..."
