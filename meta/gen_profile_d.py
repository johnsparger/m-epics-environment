#!/bin/env python2.7

"""Helper script to generate the ess_epics_env.sh file."""

from __future__ import print_function
import os
import platform
import argparse
import subprocess, shlex

SCRIPTDIR = os.path.dirname(os.path.realpath(__file__))

def get_epics_host_arch():
    """Try to figure out the host architecture based."""
    (dist, version, _) = platform.linux_distribution()
    dist = dist.lower()
    if dist:
        if 'centos' in dist:
            return 'centos{}-{}'.format(version[0], platform.machine())
        elif 'scientific linux' in dist:
            return 'SL{}-{}'.format(version[0], platform.machine())
        elif dist in ('ubuntu', 'debian'):
            return '{}{}-{}'.format(dist, version[0], platform.machine())
    return '{}-{}'.format(platform.system().lower(), platform.machine())

def get_eee_version():
    """Call make on parent folder to figure out version."""
    return subprocess.check_output(shlex.split('make --no-print-directory -C {}/../ version'.format(SCRIPTDIR))).strip()


def main():
    """Main function."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--epics-host-arch', type=str,
                        help='The EPICS Host architecture will be automatically detected if not given')
    parser.add_argument('--epics-bases-path', type=str, default='/opt/epics/bases')
    parser.add_argument('--epics-modules-path', type=str, default='/opt/epics/modules')
    parser.add_argument('--epics-base-version', type=str, default='3.15.4')
    parser.add_argument('--epics-env-version', type=str,
                        help='The EPICS Environment version will be automatically detected if not given.\
                              This only works if the script is running from its git repository.')
    parser.add_argument('--epics-pvaccesscpp-version', type=str, default='5.0.0')
    parser.add_argument('--epics-pvapy-version', type=str, default='0.6.0')
    parser.add_argument('--template-in', type=str,
                        default=os.path.join('..', 'scripts', 'epicsbootstrap', 'templates',
                                             'ess_epics_env.sh.template'))
    parser.add_argument('--template-out', type=str, default='ess_epics_env.sh')

    args = parser.parse_args()

    # Only use defaults if no argument is given
    epics_bases_path = args.epics_bases_path
    epics_modules_path = args.epics_modules_path
    epics_host_arch = args.epics_host_arch   if args.epics_host_arch   else get_epics_host_arch()
    epics_base_version = args.epics_base_version
    epics_env_version = args.epics_env_version if args.epics_env_version else get_eee_version()
    epics_pvaccesscpp_version = args.epics_pvaccesscpp_version
    epics_pvapy_version = args.epics_pvapy_version
    template_in = args.template_in
    template_in = args.template_in
    template_out = args.template_out

    with open(template_out, 'w') as profilefile, open(template_in, 'r') as templatefile:
        template = templatefile.read()
        profilefile.write(template.format(epics_host_arch=epics_host_arch,
                                          epics_bases_path=epics_bases_path,
                                          epics_modules_path=epics_modules_path,
                                          epics_base_version=epics_base_version,
                                          epics_env_version=epics_env_version,
                                          epics_pvaccesscpp_version=epics_pvaccesscpp_version,
                                          epics_pvapy_version=epics_pvapy_version))

if __name__ == "__main__":
    main()
